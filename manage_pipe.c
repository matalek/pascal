/* Program Pascal - procedura pomocniczna do podmiany standardowego
 * wejścia i wyjścia w procesie potomym - implementacja
 * Aleksander Matusiak, 347171
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "manage_pipe.h"
#include "err.h"

void prepare(int return_dsc[2], int forward_dsc[2]) {
	if (pipe(return_dsc) == -1) syserr("Error in pipe\n");
    if (pipe(forward_dsc) == -1) syserr("Error in pipe\n");
}

void manage_child(int return_dsc[2], int forward_dsc[2]) {
	//przekierowanie standardowego wejścia
	if (close(0) == -1) syserr("child, close(0)\n");
	if (dup(forward_dsc[0]) != 0) 
		syserr("child, dup(forward_dsc[0])\n");

	if (close(forward_dsc[0]) == -1) 
		syserr("child, close(forward_dsc[0])\n");
	if (close(forward_dsc[1]) == -1) 
		syserr("child, close(forward_dsc[1])\n");
		
	//przekierowanie standardowego wyjścia
	if (close(1) == -1) syserr("child, close(1)\n");
	if (dup(return_dsc[1]) != 1) 
		syserr("child, dup(return_dsc[1])\n");
	
	if (close(return_dsc[0]) == -1) 
		syserr("child, close(return_dsc[0])\n");
	if (close(return_dsc[1]) == -1) 
		syserr("child, close(return_dsc[1])\n");
	
}

void manage_parent(int return_dsc[2], int forward_dsc[2]) {
	//zamykanie nieużywanych deskryptorów
	if (close(forward_dsc[0]) == -1) 
        syserr("parent, close(forward_dsc[0])\n");
    if (close (return_dsc[1]) == -1) 
        syserr("parent, close(return_dsc[1])\n");
}
