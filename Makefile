#Program Pascal - Makefile
#Aleksander Matusiak, 347171
CC = gcc
CFLAGS = -Wall

OBJECTS = err.o manage_pipe.o
HEADERS = err.h manage_pipe.h
ALL = pascal process_w

all: $(ALL)

%.o : %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

$(ALL) : % : %.o $(OBJECTS)

clean:
	rm -f *.o $(ALL) *~
