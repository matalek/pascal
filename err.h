/* Program Pascal - obsługa błędów - interfejs
 * Aleksander Matusiak, 347171
 * źródło: http://www.mimuw.edu.pl/~janowska/SO-LAB/03_pipe/
 */

#ifndef _ERR_
#define _ERR_

/* wypisuje informacje o blednym zakonczeniu funkcji systemowej 
i konczy dzialanie */
extern void syserr(const char *fmt, ...);

/* wypisuje informacje o bledzie i konczy dzialanie */
extern void fatal(const char *fmt, ...);

#endif
