/* Program Pascal - plik procesu Pascal
 * Aleksander Matusiak, 347171
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "manage_pipe.h"
#include "err.h"

//rozmiar bufora do odczytu wyniku
#define BUF_SIZE 2048

int main(int argc, char *argv[]) {
    
    //pobieranie liczby n
    if (argc < 2) {
		fprintf(stderr, "ERROR: Agrument not stated\n");
		exit(0);
	}
    int n = atoi (argv[1]);
    if (n <= 0) {
		fprintf(stderr, "ERROR: Invalid argument\n");
		exit(0);
	}
    
    //return_dsc - łącze od potomka do macierzystego,
    //forward_dsc - od macierzystego do potomka
    int return_dsc[2], forward_dsc[2];
    prepare(return_dsc, forward_dsc);
    
    //tworzymy następny proces
    switch (fork()) {
        case -1:
            syserr("Error in fork\n");
        case 0:
            manage_child(return_dsc, forward_dsc);
            
            //wywołanie procesu potomnego
            execl("./process_w", "process_w", (char *) 0);
            syserr("Error in execl\n");
        default:
			manage_parent(return_dsc, forward_dsc);
    }
    
    //ciąg dalszy procesu macierzystego Pascal
    
    //strumień do wypisywania numeru porządkowego i kolejno 
    //wartości inicjujących 
    FILE* out_stream;
    if ((out_stream = fdopen(forward_dsc[1], "w")) == NULL)
		syserr("fdopen(forward_dsc[1], \"w\")\n");
    
    //przekazanie numeru potomka
    fprintf(out_stream, "%d\n", n);
    
    //wypisanie danych do inicjacji kolejnych kroków obliczeń
    fprintf(out_stream, "1 ");
    int i;
    for (i = 0; i < n - 1; ++i)
        fprintf(out_stream, "0 ");
    
    //zamykanie wypisywania - od razu wykonuje fflush i zamyka związany
    //deskyptor
    if (fclose(out_stream) != 0) syserr("fclose(out_stream)\n");
    
    //odczytujemy wyniki procesów
    char buf[BUF_SIZE];
    if (read(return_dsc[0], buf, BUF_SIZE - 1) == -1)
        syserr("Error in read\n");

	//zamykanie odczytu od potomka
	if (close(return_dsc[0]) == -1) syserr("close(return_dsc[0])\n");

    //wypisuję wynik
    printf("%s\n", buf);
    
    //oczekiwanie na zakończenie procesu potomnego
    if (wait(0) == -1) syserr("Error in wait\n");
    
    return 0;
}
