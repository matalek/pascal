/* Program Pascal - procedura pomocniczna do podmiany standardowego
 * wejścia i wyjścia w procesie potomym - interfejs
 * Aleksander Matusiak, 347171
 */

#ifndef _MANAGE_
#define _MANAGE_

//tworzy łącza
void prepare(int return_dsc[2], int forward_dsc[2]);

/* podmienia standardowe wejście na forward_dsc[0] oraz wyjście
 * na return_dsc[1]. Zamyka nieużywane deskryptory w procesie
 * potomnym. */
void manage_child(int return_dsc[2], int forward_dsc[2]);

//zamyka nieżywane deksryptory w procesie macierzystym 
void manage_parent(int return_dsc[2], int forward_dsc[2]);

#endif
