/* Program Pascal - plik procesu w(i)
 * Aleksander Matusiak, 347171
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "manage_pipe.h"
#include "err.h"

//rozmiar bufora do gromadzenia i wypisywania wyniku
#define BUF_SIZE 1024

int main() {
    
    //proces dostaje swój numer porządkowy: proces w(i) dostaje n-i+1
    int nr;
	scanf("%d", &nr);
    
    //bufory do odczytania ciągu wynikowego i stworzeniu nowego
    char buf[BUF_SIZE], new_buf[BUF_SIZE];
    //value - aktualna wartość, to_add - wartość do dodania
    long value = 0, to_add;
    
    //jeśli jesteśmy ostatnim procesem - w(n)
    if (nr == 1) {
        scanf("%ld", &value); //tu zawsze odczytamy 1
        new_buf[0] = '1';
        new_buf[1] = 0;
        if (write(1, new_buf, sizeof(new_buf)) == -1)
            syserr("Error in write\n");
        exit(0);
    }
    
    //return_dsc - łącze od potomka do macierzystego,
    //forward_dsc - od macierzystego do potomka
    int return_dsc[2], forward_dsc[2];
    prepare(return_dsc, forward_dsc);
    
    //tworzymy następny proces
    switch (fork()) {
        case -1:
            syserr("Error in fork\n");
        case 0:
            //przekierowanie standardowego wejścia i wyjścia
            manage_child(return_dsc, forward_dsc);
            
            //wywołanie procesu potomnego
            execl("./process_w", "process_w", (char *) 0);
            syserr("Error in execl\n");
        default:
			manage_parent(return_dsc, forward_dsc);
    }
    
    //ciąg dalszy procesu macierzystego
    
    //strumień do wypisywania numeru prządkowego potomka oraz kolejno
    //swoich starych wartości
    FILE* out_stream;
    if ((out_stream = fdopen(forward_dsc[1], "w")) == NULL)
		syserr("fdopen(forward_dsc[1], \"w\")\n");
    
    //przekazanie numeru potomka
    fprintf(out_stream, "%d\n", nr - 1);
    
    //odczytywanie wartości wcześniejszych procesów, przekazywanie i 
    //aktualizacja własnej wartości
    int i;
    for (i = 0; i < nr; ++i) {
        scanf("%ld", &to_add);
        if (value != 0) fprintf(out_stream, "%ld\n", value);
        value += to_add;
    }
	
	//zamykanie odczytu od przodka
	if (close(0) == -1) syserr("close(0)\n");
	
    //zamykanie wypisywania do potomka - od razu wykonuje fflush
    //i zamyka związany deskyptor
    if (fclose(out_stream) != 0) syserr("fclose(out_stream)\n");
    
    //odczytujemy ciąg wartości od następnych procesów
    if (read(return_dsc[0], buf, BUF_SIZE - 1) == -1)
        syserr("Error in read\n");

	//zamykanie odczytu od potomka
	if (close(return_dsc[0]) == -1) syserr("close(return_dsc[0])\n");

    //dodajemy swoją i przekazujemy dalej
    snprintf(new_buf, sizeof(new_buf), "%s %ld", buf, value);
    if (write(1, new_buf, sizeof(new_buf)) == -1)
        syserr("Error in write\n");
    
    //zamykanie zapisu do przodka
	if (close(1) == -1) syserr("close(1)\n");
    
    //oczekiwanie na zakończenie procesu potomnego
    if (wait(0) == -1)  syserr("Error in wait\n");

    exit(0);
}
